<?php
// src/AppBundlle/Validator/ProjetValidator

namespace AppBundlle\Validator;

use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ProjetValidator {
    public static function validate($object, ExecutionContextInterface $context, $payload)
    {
        // récupération des données renvoyées par l'utilisateur
        $data = $context->getObject();

        // récupération de la date

        $date = $data->getDateStart();
        // création de la date du jour + 7 jours
        $limit = new DateTime();
        $limit->add(new DateInterval('P7D'));


        // vérification que la date de l'utilisateur est postérieure à la date limite
        if ($date < $limit) {
        $context->buildViolation("Veuillez renseigner une date postérieure à J + 7")
            ->atPath('dateStart')
            ->addViolation()
        ;
        }
    }
}

